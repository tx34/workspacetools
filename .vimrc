set nocompatible
set number " line number
set ruler " cursor position
set showcmd
set showmode
set mouse=a " allow mouse control
set matchpairs+=<:> " match triangle brackets
set backspace=indent,eol,start " allow more deletions in insert mode

set hlsearch " highlight search
set incsearch " incrementally search
set ignorecase " ignore case on search
set smartcase " case sensitive search with caps
set wrapscan " search wraps around the file
"set showmatch " show match by jumping cursor

set autoindent " use same indentation on next line
set smartindent " indent with scope
set tabstop=3 " 3 spaces per tab
set expandtab " replace tabs with spaces
set shiftwidth=3 " 3 spaces per << >>
set virtualedit=all " allow the cursor in invalid position
" fileformat lineendings set ff=dos unix
" save line endings :w ++ff=unix

" let mapleader=" "
" map normal visual select operator modes
" map! insert and command modes
" nore do not use recursive mapping
noremap <C-c> "+y
noremap <C-x> "+d
noremap <C-v> "+p
noremap! <C-v> <C-r>+
noremap <leader>y "+y
noremap <leader>p "+p
noremap <leader>d "+d

vnoremap // y/<C-r>"<CR>
nnoremap <leader>v <C-v>
nnoremap Y y$

" VsVim
"vmap <leader>/ :vsc Edit.CommentSelection<CR>
"vmap <leader>? :vsc Edit.UncommentSelection<CR>
"cmap :sp :vsc Window.NewHorizontalTabGroup
"cmap :vsp :vsc Window.NewVerticalTabGroup
"nmap <C-w>h :vsc Window.PreviousTab<CR>
"nmap <C-w>l :vsc Window.NextTab<CR>
"nmap <C-w>j :vsc Window.MoveToNextTabGroup<CR>
"nmap <C-w>k :vsc Window.MoveToPreviousTabGroup<CR>
"nmap <C-O> :vsc View.NavigateBackward<CR>
"nmap <C-I> :vsc View.NavigateForward<CR>
